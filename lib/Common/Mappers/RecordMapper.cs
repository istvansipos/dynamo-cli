﻿
using Amazon.DynamoDBv2.Model;
using Dynamo.Common.Field;
using System;
using System.Collections.Generic;

namespace Dynamo.Common.Mappers
{
    using Record = Dictionary<string, AttributeValue>;

    public static class RecordMapper
    {
        public  static Record FromStrings(FieldDefinition[] attributes, string[] values)
        {
            var record = new Record();
            for (var i = 0; i < Math.Min(attributes.Length, values.Length); i++)
            {
                if (string.IsNullOrWhiteSpace(values[i])) continue;
                var attributeValue = AttributeMapper.FromString(attributes[i].Type, values[i]);
                record.Add(attributes[i].Name, attributeValue);
            }
            return record;
        }
    }
}
