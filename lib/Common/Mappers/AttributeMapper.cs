﻿using Amazon.DynamoDBv2.Model;
using Dynamo.Common.Field;
using System;
using System.IO;

namespace Dynamo.Common.Mappers
{
    public static class AttributeMapper
    {
        public static string AsString(FieldType type, AttributeValue value)
        {
            return type switch
            {
                FieldType.B => Convert.ToBase64String(value.B.ToArray()),
                FieldType.N => value.N.ToString(),
                FieldType.S => value.S,
                FieldType.BOOL => value.BOOL ? "true" : "false",
                _ => throw new NotImplementedException($"Field Type not supported: {Enum.GetName(type)}"),
            };
        }

        public static AttributeValue FromString(FieldType type, string value)
        {
            var attributeValue = new AttributeValue();
            switch (type)
            {
                case FieldType.B:
                    attributeValue.B = new MemoryStream(Convert.FromBase64String(value));
                    break;
                case FieldType.N:
                    attributeValue.N = value;
                    break;
                case FieldType.S:
                    attributeValue.S = value;
                    break;
                case FieldType.BOOL:
                    attributeValue.BOOL = value.Equals("true");
                    break;
                default:
                    throw new NotImplementedException($"Field Type not supported: {Enum.GetName(type)}");
            }

            return attributeValue;
        }
    }
}
