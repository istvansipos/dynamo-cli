﻿
using System;

namespace Dynamo.Common.Field
{
    public record FieldDefinition(string Name, FieldType Type)
    {
        public override string ToString()
        {
            return $"{Name}:{Enum.GetName(Type)}";
        }

        public static implicit operator string(FieldDefinition fieldDefinition) => fieldDefinition.ToString();
        public static implicit operator FieldDefinition(string value)
        {
            var parts = value.Split(':', 2);
            var name = parts[0];
            var type = parts.Length < 2 ? FieldType.S : Enum.Parse<FieldType>(parts[1]);

            return new FieldDefinition(name, type);
        }
    }
}
