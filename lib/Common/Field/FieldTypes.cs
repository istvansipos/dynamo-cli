﻿namespace Dynamo.Common.Field
{
    public enum FieldType
    {
        S,
        SS,
        B,
        BS,
        N,
        NS,
        BOOL
    }
}
