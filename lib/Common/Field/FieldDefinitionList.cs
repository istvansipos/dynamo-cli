﻿using System.Collections.Generic;
using System.Linq;

namespace Dynamo.Common.Field
{
    public class FieldDefinitionList : List<FieldDefinition>
    {
        public override string ToString()
        {
            return string.Join(',', this.Select(x=>x.ToString()).ToArray());
        }

        public static implicit operator string(FieldDefinitionList fieldDefinitionList) => fieldDefinitionList.ToString();
        public static implicit operator FieldDefinitionList(string value)
        {
            var result = new FieldDefinitionList();
            foreach (var fieldDefinition in value.Split(','))
                result.Add(fieldDefinition);
            return result;
        }

    }
}
