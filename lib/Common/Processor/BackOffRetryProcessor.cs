﻿using Dynamo.Common.Pocessor;
using System.Threading;
using System.Threading.Tasks;

namespace Dynamo.Common.Processor
{
    public class BackOffRetryProcessor<T> : IProcessor<T>
    {
        private readonly int[] _backOffDelays;
        private readonly IProcessor<T> _processor;
        private readonly CancellationToken _cancellationToken;

        public BackOffRetryProcessor(IProcessor<T> processor, int[] backOffDelays, CancellationToken cancellationToken)
        {
            _processor = processor;
            _backOffDelays = backOffDelays;
            _cancellationToken = cancellationToken;
        }

        public async Task<bool> Process(T payload)
        {
            int retries = 0;
            while (retries < _backOffDelays.Length)
            {
                if (_backOffDelays[retries] > 0)
                    await Task.Delay(_backOffDelays[retries]);
                if (_cancellationToken.IsCancellationRequested) return false;
                var result = await _processor.Process(payload);
                
                if (result) return true;
            }
            return false;
        }

        public Task WaitAll() => _processor.WaitAll();

        public void Dispose()
        {
            // Nothing to dispose
        }
    }
}
