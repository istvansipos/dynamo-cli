﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Dynamo.Common.Pocessor
{
    public class BatchedProcessor<T> : IProcessor<T>
    {
        private bool _disposed = false;
        private readonly CancellationToken _cancellationToken;
        private readonly int _batchSize;
        private readonly SemaphoreSlim _semaphoreSlim;
        private List<T> _batch;
        private readonly IProcessor<List<T>> _processor;

        public BatchedProcessor(IProcessor<List<T>> processor, int batchSize, CancellationToken cancellationToken)
        {
            _batchSize = batchSize;
            _cancellationToken = cancellationToken;
            _processor = processor;
            _semaphoreSlim = new SemaphoreSlim(1);
        }

        public async Task<bool> Process(T payload)
        {
            await _semaphoreSlim.WaitAsync(_cancellationToken);
            try
            {
                if (_cancellationToken.IsCancellationRequested) return true;

                if (_batch is null) _batch = new List<T>();
                _batch.Add(payload);

                await Flush(_batchSize);
            }
            finally
            {
                _semaphoreSlim.Release();
            }

            return true;
        }

        public async Task WaitAll()
        {
            await _semaphoreSlim.WaitAsync(_cancellationToken);

            try{
                if (_cancellationToken.IsCancellationRequested) return;

                await Flush();
                await _processor.WaitAll();
            }
            finally
            {
                _semaphoreSlim.Release();
            }          
        }

        private async Task Flush(int limit = 0)
        {
            List<T> currentBatch = null;
            
            if (_batch is null) return;
            if (_batch.Count >= limit)
            {
                currentBatch = _batch;
                _batch = null;
            }

            if (currentBatch is null) return;

            await _processor.Process(currentBatch);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                _semaphoreSlim?.Dispose();
            }

            _disposed = true;
        }
    }
}
