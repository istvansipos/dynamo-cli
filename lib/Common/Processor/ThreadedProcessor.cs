using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Dynamo.Common.Pocessor
{
    public class ThreadedProcessor<T> : IProcessor<T>
    {
        private bool _disposed = false;
        private readonly CancellationToken _cancellationToken;
        private readonly SemaphoreSlim _semaphoreSlim;
        private readonly List<Task> _tasks;
        private readonly IProcessor<T> _processor;

        public ThreadedProcessor(IProcessor<T> processor, int threads, CancellationToken cancellationToken)
        {
            _processor = processor;
            _cancellationToken = cancellationToken;
            _semaphoreSlim = new SemaphoreSlim(threads);
            _tasks = new List<Task>();
        }

        public async Task WaitAll()
        {
            await _processor.WaitAll();
            Task.WaitAll(_tasks.ToArray());
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                _semaphoreSlim?.Dispose();
            }

            _disposed = true;
        }

        public async Task<bool> Process(T payload)
        {
            await _semaphoreSlim.WaitAsync(_cancellationToken);
            if (_cancellationToken.IsCancellationRequested)
            {
                _semaphoreSlim.Release();
                return false;
            }

            var task = Task.Factory.StartNew(async () =>
            {
                try
                {
                    await _processor.Process(payload);
                }
                finally
                {
                    _semaphoreSlim.Release();
                }
            }, _cancellationToken).Unwrap();

            _tasks.Add(task);
            return true;
        }
    }
}
