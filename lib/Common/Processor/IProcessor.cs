﻿using System;
using System.Threading.Tasks;

namespace Dynamo.Common.Pocessor
{
    public interface IProcessor<T> : IDisposable
    {
        Task<bool> Process(T payload);
        Task WaitAll();
    }
}
