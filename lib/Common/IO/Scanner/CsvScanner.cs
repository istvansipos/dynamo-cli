
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using CsvHelper;

namespace Dynamo.Common.IO.Scanner
{

    public class CsvScanner : IDisposable
    {
        private bool _disposed = false;
        private readonly CsvReader _csvReader;

        public CsvScanner(TextReader textReader)
        {
            _csvReader = new CsvReader(textReader, CultureInfo.InvariantCulture);
        }
        public IEnumerable<IEnumerable<string>> Rows
        {
            get
            {
                while (_csvReader.Read())
                {
                    var row = new List<string>();
                    int index = 0;
                    while (true)
                    {
                        if (!_csvReader.TryGetField<string>(index, out string value)) break;
                        row.Add(value);
                        index += 1;
                    }

                    yield return row;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                _csvReader?.Dispose();
            }

            _disposed = true;
        }
    }
}