
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using CsvHelper;

namespace Dynamo.Common.IO.Printer
{

    public class CsvPrinter : IDisposable
    {
        private bool _disposed = false;

        private readonly CsvWriter _csvWriter;

        public CsvPrinter(TextWriter textWriter)
        {
            _csvWriter = new CsvWriter(textWriter, CultureInfo.InvariantCulture);
        }

        public void WriteRow(IEnumerable<string> row)
        {
            foreach (var field in row)
            {
                _csvWriter.WriteField(field);
            }
            _csvWriter.NextRecord();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                _csvWriter?.Dispose();
            }

            _disposed = true;
        }
    }
}