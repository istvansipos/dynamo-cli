﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using CommandLine;
using Dynamo.Application.Cli.Operations;
using Dynamo.Common.Field;

namespace Dynamo.Application.Cli
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var cancellationTokenSource = Bootstrap();

            await Parser
              .Default
              .ParseArguments<CommandLineOptions>(args)
              .MapResult(async (opts) =>
                {
                    var attributes = ((FieldDefinitionList)opts.Attributes).ToArray();
                    switch (opts.Operation)
                    {
                        case "delete":
                            await DeleteOperation.Delete(opts.TableName, attributes, opts.Threads, cancellationTokenSource.Token);
                            break;
                        case "put":
                            await PutOperation.Put(opts.TableName, attributes, opts.Threads, cancellationTokenSource.Token);
                            break;
                        case "scan":
                            await ScanOperation.Scan(opts.TableName, attributes, opts.TotalSegments, opts.Segment, cancellationTokenSource.Token);
                            break;
                        default:
                            throw new InvalidOperationException(opts.Operation);
                    }
                },
                errors =>
                {
                    foreach (var error in errors)
                        Console.Error.WriteLine(error.ToString());
                    return Task.CompletedTask;
                }
              );
        }

        private static CancellationTokenSource Bootstrap()
        {
            var cts = new CancellationTokenSource();

            Console.CancelKeyPress += (s, e) =>
            {
                cts.Cancel();
                e.Cancel = true;
            };
            return cts;
        }
    }
}
