﻿
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Dynamo.Common.Pocessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Dynamo.Application.Cli.Processor
{
    using Batch = List<Dictionary<string, AttributeValue>>;
    public class WriteItemProcessor<T> : IProcessor<Batch>
        where T : class
    {
        private readonly CancellationToken _cancellationToken;
        private readonly AmazonDynamoDBClient _client;
        private readonly string _tableName;

        public WriteItemProcessor(AmazonDynamoDBClient client, string tableName, CancellationToken cancellationToken)
        {
            _client = client;
            _tableName = tableName;
            _cancellationToken = cancellationToken;
        }

        public async Task<bool> Process(Batch batch)
        {
            try
            {
                var batchWriteItemResponse = await SendWriteRequest(batch);
                return (batchWriteItemResponse.HttpStatusCode == HttpStatusCode.OK
                    && batchWriteItemResponse.UnprocessedItems.Count == 0);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
                return false;
            }
        }
        private Task<BatchWriteItemResponse> SendWriteRequest(Batch batch)
        {
            var writeRequests = new List<WriteRequest>();
            foreach (var item in batch)
            {
                WriteRequest writeRequest;
                var t = typeof(T);
                if (t.IsAssignableFrom(typeof(DeleteRequest)))
                {
                    var request = new DeleteRequest(item.ToDictionary(x => x.Key, x => x.Value));
                    writeRequest = new WriteRequest(request);
                }
                else if (t.IsAssignableFrom(typeof(PutRequest)))
                {
                    var request = new PutRequest(item.ToDictionary(x => x.Key, x => x.Value));
                    writeRequest = new WriteRequest(request);
                }
                else
                {
                    throw new NotSupportedException($"Type parameter not supported: {t.FullName}");
                }

                writeRequests.Add(writeRequest);
            }

            var batchWriteItemRequest = new BatchWriteItemRequest(new() { [_tableName] = writeRequests });

            return _client.BatchWriteItemAsync(batchWriteItemRequest, _cancellationToken);
        }

        public Task WaitAll()
        {
            // Nothing to wait for
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            // Nothing to dispose
        }
    }
}
