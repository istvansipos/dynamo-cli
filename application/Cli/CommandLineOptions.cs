using CommandLine;

namespace Dynamo.Application.Cli
{
    class CommandLineOptions
    {
        [Value(index: 0, Required = true, HelpText = "Operation to run (put|delete|scan)")]
        public string Operation { get; set; }

        [Value(index: 1, Required = true, HelpText = "DynamoDB table name")]
        public string TableName { get; set; }

        [Value(index: 2, Required = true, HelpText = "Comma separated list of attribute definitions")]
        public string Attributes { get; set; }

        [Option(longName: "threads", shortName: 't', Default = 1, HelpText = "Number of concurrent threads used (only for put and delete)")]
        public int Threads { get; set; }

        [Option(longName: "total-segments", shortName: 'n', Default = 1, HelpText = "Total segments (only for scan)")]
        public int TotalSegments { get; set; }

        [Option(longName: "segments", shortName: 's', Default = 0, HelpText = "Segment (only for scan)")]
        public int Segment { get; set; }
    }
}

