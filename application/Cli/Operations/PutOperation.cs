﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Dynamo.Application.Cli.Processor;
using Dynamo.Common.Field;
using Dynamo.Common.IO.Scanner;
using Dynamo.Common.Mappers;
using Dynamo.Common.Pocessor;
using Dynamo.Common.Processor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Dynamo.Application.Cli.Operations
{
    using Record = Dictionary<string, AttributeValue>;
    using Batch = List<Dictionary<string, AttributeValue>>;

    static class PutOperation
    {
        public static async Task Put(string tableName, FieldDefinition[] attributes, int threads, CancellationToken cancellationToken)
        {
            var client = new AmazonDynamoDBClient();
            var backOffRetryDelays = new int[] { 0, 100, 300, 1000, 3000, 10000 };

            using var putProcessor = new WriteItemProcessor<PutRequest>(client, tableName, cancellationToken);
            using var backOffRetryProcessor = new BackOffRetryProcessor<Batch>(putProcessor, backOffRetryDelays, cancellationToken);
            using var batchedProcessor = new BatchedProcessor<Record>(backOffRetryProcessor, 25, cancellationToken);
            using var processor = new ThreadedProcessor<Record>(batchedProcessor, threads, cancellationToken);
            using var csvScanner = new CsvScanner(Console.In);
            foreach (var row in csvScanner.Rows)
            {
                if (cancellationToken.IsCancellationRequested) break;
                var record = RecordMapper.FromStrings(attributes, row.ToArray());
                await processor.Process(record);
            }
            await processor.WaitAll();
        }
    }
}
