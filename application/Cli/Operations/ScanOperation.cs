﻿
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Dynamo.Common.Field;
using Dynamo.Common.IO.Printer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Dynamo.Application.Cli.Operations
{
    public static class ScanOperation
    {
        public static async Task Scan(string tableName, FieldDefinition[] attributes, int totalSegments, int segment, CancellationToken cancellationToken)
        {
            var client = new AmazonDynamoDBClient();

            var table = Table.LoadTable(client, tableName);

            using var printer = new CsvPrinter(Console.Out);
            await foreach (var values in Scan(table, attributes, totalSegments, segment))
            {
                if (cancellationToken.IsCancellationRequested) break;
                printer.WriteRow(values);
            }
        }

        static async IAsyncEnumerable<IEnumerable<string>> Scan(Table table, FieldDefinition[] attributes, int totalSegments, int segment)
        {
            var attributesToGet = attributes.Select(x => x.Name).ToList();

            ScanOperationConfig config = new()
            {
                Select = SelectValues.SpecificAttributes,
                AttributesToGet = attributesToGet,
                TotalSegments = totalSegments,
                Segment = segment
            };

            var search = table.Scan(config);
            do
            {
                var documentList = await search.GetNextSetAsync();
                foreach (var document in documentList)
                {
                    yield return ToList(document, attributes);
                }
            } while (!search.IsDone);
        }

        private static List<string> ToList(Document document, FieldDefinition[] attributes)
        {
            var values = new List<string>();
            foreach (var attribute in attributes)
            {
                var value = string.Empty;

                if (document.ContainsKey(attribute.Name))
                {
                    value = attribute.Type switch
                    {
                        FieldType.B => Convert.ToBase64String(document[attribute.Name].AsByteArray()),
                        FieldType.N or FieldType.S => document[attribute.Name].AsString(),
                        FieldType.BOOL => document[attribute.Name].AsBoolean() ? "true" : "false",
                        _ => throw new NotImplementedException($"Field Type not supported: {Enum.GetName(attribute.Type)}"),
                    };
                }

                values.Add(value);
            }

            return values;
        }
    }
}
